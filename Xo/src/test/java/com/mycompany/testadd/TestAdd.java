/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.testadd;

import com.mycompany.xo.Xo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HotLapTop
 */
public class TestAdd {
    
    public TestAdd() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void test_Add_Num1_1_Num2_6_Output_7() {
        int result = Xo.AddNum(4, 6);
        assertEquals(10,result);
    }
    
    @Test
    public void test_Add_Num1_4_Num2_6_Output_10() {
        int result = Xo.AddNum(4, 6);
        assertEquals(10, result);
    }
       
}
