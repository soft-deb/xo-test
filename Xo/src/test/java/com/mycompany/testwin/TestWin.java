/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.testwin;

import com.mycompany.xo.Xo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HotLapTop
 */
public class TestWin {
    
    public TestWin() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void test_isWinOnRow_True() {
        Xo.createTable();
        Xo.testTableTrue();
        boolean result = Xo.isWinOnRow();
        assertTrue(result);
    }

    @Test
    public void test_isWinOnCol_True() {
        Xo.createTable();
        Xo.testTableTrue();
        boolean result = Xo.isWinOnCol();
        assertTrue(result);
    }

        @Test
    public void test_isWinOnCross_True() {
        Xo.createTable();
        Xo.testTableTrue();
        boolean result = Xo.isWinOnCross();
        assertTrue(result);
    }

    @Test
    public void test_isWin_True() {
        Xo.createTable();
        Xo.testTableTrue();
        boolean result = Xo.isWin();
        assertTrue(result);
    }

    @Test
    public void test_isWinOnRow_False() {
        Xo.createTable();
        Xo.testTableFalse();
        boolean result = Xo.isWinOnRow();
        assertFalse(result);
    }

    @Test
    public void test_isWinOnCol_False() {
        Xo.createTable();
        Xo.testTableFalse();
        boolean result = Xo.isWinOnCol();
        assertFalse(result);
    }

    @Test
    public void test_isWinOnCross_False() {
        Xo.createTable();
        Xo.testTableFalse();
        boolean result = Xo.isWinOnCross();
        assertFalse(result);
    }

    @Test
    public void test_isWin_False() {
        Xo.createTable();
        Xo.testTableFalse();
        boolean result = Xo.isWin();
        assertFalse(result);
    }
    
        @Test
    public void test_isDraw_True() {
        Xo.createTable();
        Xo.testTableFalse();
        boolean result = Xo.isDraw();
        assertTrue(result);
    }

    @Test
    public void test_isDraw_False() {
        Xo.createTable();
        boolean result = Xo.isDraw();
        assertFalse(result);
    }

}
