/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xo;

import java.util.Scanner;

public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        while (true) {
            boolean isFinish = false;
            printWelcome();
            newGame();
            while (!isFinish) {
                printTable();
                printTurn();
                if (inputRowCol()) {
                    continue;
                }
                if (table.checkWin() || table.checkDraw()) {
                    printTable();
                    showEnding();
                    ShowPlayer();
                    isFinish = true;
                }
                table.switchPlayer();
            }
            if (isContinue()) {
                break;
            }
        }
    }

    private void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private boolean inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input row col:");
        int row = kb.nextInt();
        int col = kb.nextInt();
        return table.setRowCol(row - 1, col - 1);
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void showEnding() {
        if (table.checkWin()) {
            System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!");
        } else {
            System.out.println("Draw!");
        }
    }

    private void ShowPlayer() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private boolean isContinue() {
        Scanner kb = new Scanner(System.in);
        String choice;
        while (true) {
            System.out.print("Please input continue(y/n): ");
            choice = kb.next().toLowerCase();
            if (choice.equals("n")) {
                return true;
            } else if (choice.equals("y")) {
                return false;
            } else {
                System.out.println("Please enter y or n");
            }
        }

    }
}


