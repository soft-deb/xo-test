package com.mycompany.xo;

import java.util.Scanner;

public class Xo {
    static int row,col;
    static char[][] table;
    static char currentPlayer = 'X';
    static Scanner kb = new Scanner(System.in);

    
    public static int AddNum(int num1, int num2) {
        return num1 + num2;
    }
    
    public static void createTable() {
        table = new char[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                table[r][c] = '-';
            }
        }
    }

        public static void testTableTrue() {
        table = new char[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                table[r][c] = 'X';
            }
        }
    }

    public static void testTableFalse() {
        table = new char[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                table[r][c] = 'O';
            }
        }
    }

    
    public static void printTable() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println();
        }
    }

    public static void printTitle() {
        System.out.println("Welcome to XO");
    }

    public static void printPlayerTurn() {
        System.out.println(currentPlayer + " turn");
    }

    public static boolean isWinOnRow() {
        for (int r = 0; r < 3; r++) {
            if (table[r][0] == currentPlayer && table[r][1] == currentPlayer && table[r][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWinOnCol() {
        for (int c = 0; c < 3; c++) {
            if (table[0][c] == currentPlayer && table[1][c] == currentPlayer && table[2][c] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWinOnCross() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer;
    }

    public static boolean isWin() {
        return (isWinOnRow() || isWinOnCol() || isWinOnCross()) ? true : false;
    }

    public static boolean isDraw() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (table[r][c] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isContinue() {
        String choice;
        while (true) {
            System.out.print("Please input continue(y/n): ");
            choice = kb.next().toLowerCase();
            if (choice.equals("n")) {
                return false;
            } else if (choice.equals("y")) {
                break;
            } else {
                System.out.println("Please enter y or n");
            }
        }
        return true;
    }

    public static boolean isOutOfBound(int row, int col) {
        return row < 0 || row >= 3 || col < 0 || col >= 3;
    }

    public static boolean isNotEmpty(int row, int col) {
        return table[row][col] != '-';
    }

    public static void insertRowCol() {
        System.out.print("Please input row col(1-3): ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
    }

    public static boolean hasEnd() {
        if (isWin()) {
            System.out.println(currentPlayer + " is a winner!");
            return true;
        } else if (isDraw()) {
            System.out.println("The game ends in a draw!");
            return true;
        }
        return false;
    }

    public static boolean isInvalidMove(int row, int col) {
        if (isOutOfBound(row, col) || isNotEmpty(row, col)) {
            System.out.println("Invalid move. Try again.");
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        createTable();
        while (true) {
            printTitle();
            printTable();
            while (true){
                printPlayerTurn();
                insertRowCol();
                if (isInvalidMove(row, col)) continue;
                table[row][col] = currentPlayer;
                printTable();
                if (hasEnd()) break;
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
            }
            if (!isContinue()) break;
        }
    }
}
