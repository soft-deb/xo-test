/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xo;

/**
 *
 * @author HotLapTop
 */
public class Table {
    
    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1, player2, currentPlayer;
    private int row, col, count;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
        count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (isOutOfbound(row, col) || isNotEmpty(row, col)) {
        return true;
        } else {
            table[row][col] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return false;
        }
    }

    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }
        if (checkCol()) {
            saveWin();
            return true;
        }
        if (checkCross()) {
            saveWin();
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        if (count == 9) {
            player1.draw();
            player2.draw();
            return true;
        }
        return false;
    }

    public boolean checkRow() {
        return table[row][0] != '-' && table[row][0] == table[row][1] && table[row][0] == table[row][2];
    }

    public boolean checkCol() {
        return table[0][col] != '-' && table[0][col] == table[1][col] && table[0][col] == table[2][col];
    }

    public boolean checkCross() {
        return (table[0][0] != '-' && table[0][0] == table[1][1] && table[0][0] == table[2][2]) || (table[0][2] != '-' && table[0][2] == table[1][1] && table[0][2] == table[2][0]);
    }

    private void saveWin() {
        if (currentPlayer == player1) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    public void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    private boolean isNotEmpty(int row, int col) {
        return table[row][col] != '-';
    }

    private boolean isOutOfbound(int row, int col) {
        return row < 0 || row >= 3 || col < 0 || col >= 3;
    }
    
}
