package com.mycompany.xo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XOGUI extends javax.swing.JFrame {

    public XOGUI() {
        initComponents();
        this.player1 = new Player('X');
        this.player2 = new Player('O');
        load();
        showPlayer();
        newGame();
    }

    public void load() {
    FileInputStream fis = null;
        try {
            File file = new File("player.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.player2 = (Player) ois.readObject();
            this.player1 = (Player) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception ex) {
            System.out.println("Load Error");
        } finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(XOGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
public void save(){
    FileOutputStream fos = null;
        try {
            File file = new File("player.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(player2);
            oos.writeObject(player1);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println("Write Error");
        } finally {
            try {
                if(fos!=null){
                    fos.close();

                }
                            } catch (IOException ex) {
                Logger.getLogger(XOGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Table = new javax.swing.JPanel();
        btnTable00 = new javax.swing.JButton();
        btnTable01 = new javax.swing.JButton();
        btnTable02 = new javax.swing.JButton();
        btnTable10 = new javax.swing.JButton();
        btnTable11 = new javax.swing.JButton();
        btnTable12 = new javax.swing.JButton();
        btnTable20 = new javax.swing.JButton();
        btnTable21 = new javax.swing.JButton();
        btnTable22 = new javax.swing.JButton();
        Stat = new javax.swing.JPanel();
        txtPlayerX = new javax.swing.JLabel();
        txtPlayerO = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        txtPlayerXWin = new javax.swing.JLabel();
        txtPlayerXLose = new javax.swing.JLabel();
        txtPlayerXDraw = new javax.swing.JLabel();
        txtPlayerOWin = new javax.swing.JLabel();
        txtPlayerOLose = new javax.swing.JLabel();
        txtPlayerODraw = new javax.swing.JLabel();
        Status = new javax.swing.JPanel();
        txtStatus = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("XO Game");

        Table.setBackground(new java.awt.Color(204, 255, 204));

        btnTable00.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable00.setText("-");
        btnTable00.setRequestFocusEnabled(false);
        btnTable00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable00ActionPerformed(evt);
            }
        });

        btnTable01.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable01.setText("-");
        btnTable01.setRequestFocusEnabled(false);
        btnTable01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable01ActionPerformed(evt);
            }
        });

        btnTable02.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable02.setText("-");
        btnTable02.setRequestFocusEnabled(false);
        btnTable02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable02ActionPerformed(evt);
            }
        });

        btnTable10.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable10.setText("-");
        btnTable10.setRequestFocusEnabled(false);
        btnTable10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable10ActionPerformed(evt);
            }
        });

        btnTable11.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable11.setText("-");
        btnTable11.setRequestFocusEnabled(false);
        btnTable11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable11ActionPerformed(evt);
            }
        });

        btnTable12.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable12.setText("-");
        btnTable12.setRequestFocusEnabled(false);
        btnTable12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable12ActionPerformed(evt);
            }
        });

        btnTable20.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable20.setText("-");
        btnTable20.setRequestFocusEnabled(false);
        btnTable20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable20ActionPerformed(evt);
            }
        });

        btnTable21.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable21.setText("-");
        btnTable21.setRequestFocusEnabled(false);
        btnTable21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable21ActionPerformed(evt);
            }
        });

        btnTable22.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnTable22.setText("-");
        btnTable22.setRequestFocusEnabled(false);
        btnTable22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable22ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout TableLayout = new javax.swing.GroupLayout(Table);
        Table.setLayout(TableLayout);
        TableLayout.setHorizontalGroup(
            TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TableLayout.createSequentialGroup()
                        .addComponent(btnTable20, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTable21, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTable22, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(TableLayout.createSequentialGroup()
                        .addGroup(TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(TableLayout.createSequentialGroup()
                                .addComponent(btnTable00, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable01, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable02, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(TableLayout.createSequentialGroup()
                                .addComponent(btnTable10, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable11, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable12, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(12, Short.MAX_VALUE))))
        );
        TableLayout.setVerticalGroup(
            TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable01, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable02, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable00, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable10, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable11, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable12, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable20, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable21, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable22, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        Stat.setBackground(new java.awt.Color(204, 204, 255));

        txtPlayerX.setText("Player : X");

        txtPlayerO.setText("Player : O");

        btnNewGame.setText("New Game");
        btnNewGame.setRequestFocusEnabled(false);
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });

        txtPlayerXWin.setText("Win: ");

        txtPlayerXLose.setText("Lose: ");

        txtPlayerXDraw.setText("Draw:");

        txtPlayerOWin.setText("Win: ");

        txtPlayerOLose.setText("Lose: ");

        txtPlayerODraw.setText("Draw:");

        javax.swing.GroupLayout StatLayout = new javax.swing.GroupLayout(Stat);
        Stat.setLayout(StatLayout);
        StatLayout.setHorizontalGroup(
            StatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StatLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(StatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNewGame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(StatLayout.createSequentialGroup()
                        .addComponent(txtPlayerXWin, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPlayerXLose, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPlayerXDraw, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE))
                    .addGroup(StatLayout.createSequentialGroup()
                        .addGroup(StatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPlayerX, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPlayerO))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(StatLayout.createSequentialGroup()
                        .addComponent(txtPlayerOWin, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPlayerOLose, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPlayerODraw, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        StatLayout.setVerticalGroup(
            StatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StatLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtPlayerX)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(StatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPlayerXWin)
                    .addComponent(txtPlayerXLose)
                    .addComponent(txtPlayerXDraw))
                .addGap(61, 61, 61)
                .addComponent(txtPlayerO)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(StatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPlayerOWin)
                    .addComponent(txtPlayerOLose)
                    .addComponent(txtPlayerODraw))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        Status.setBackground(new java.awt.Color(255, 255, 204));

        txtStatus.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtStatus.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        txtStatus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        txtStatus.setRequestFocusEnabled(false);

        javax.swing.GroupLayout StatusLayout = new javax.swing.GroupLayout(Status);
        Status.setLayout(StatusLayout);
        StatusLayout.setHorizontalGroup(
            StatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StatusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        StatusLayout.setVerticalGroup(
            StatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StatusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Table, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Stat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(Status, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Table, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Stat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTable00ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable00ActionPerformed
        process(0, 0);
    }//GEN-LAST:event_btnTable00ActionPerformed

    private void btnTable12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable12ActionPerformed
        process(1, 2);
    }//GEN-LAST:event_btnTable12ActionPerformed

    private void btnTable01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable01ActionPerformed
        process(0, 1);
    }//GEN-LAST:event_btnTable01ActionPerformed

    private void btnTable02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable02ActionPerformed
        process(0, 2);
    }//GEN-LAST:event_btnTable02ActionPerformed

    private void btnTable10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable10ActionPerformed
        process(1, 0);
    }//GEN-LAST:event_btnTable10ActionPerformed

    private void btnTable11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable11ActionPerformed
        process(1, 1);
    }//GEN-LAST:event_btnTable11ActionPerformed

    private void btnTable20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable20ActionPerformed
        process(2, 0);
    }//GEN-LAST:event_btnTable20ActionPerformed

    private void btnTable21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable21ActionPerformed
        process(2, 1);
    }//GEN-LAST:event_btnTable21ActionPerformed

    private void btnTable22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable22ActionPerformed
        process(2, 2);
    }//GEN-LAST:event_btnTable22ActionPerformed

    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed
        newGame();
    }//GEN-LAST:event_btnNewGameActionPerformed

  public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(XOGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(XOGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(XOGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(XOGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new XOGUI().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Stat;
    private javax.swing.JPanel Status;
    private javax.swing.JPanel Table;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnTable00;
    private javax.swing.JButton btnTable01;
    private javax.swing.JButton btnTable02;
    private javax.swing.JButton btnTable10;
    private javax.swing.JButton btnTable11;
    private javax.swing.JButton btnTable12;
    private javax.swing.JButton btnTable20;
    private javax.swing.JButton btnTable21;
    private javax.swing.JButton btnTable22;
    private javax.swing.JLabel txtPlayerO;
    private javax.swing.JLabel txtPlayerODraw;
    private javax.swing.JLabel txtPlayerOLose;
    private javax.swing.JLabel txtPlayerOWin;
    private javax.swing.JLabel txtPlayerX;
    private javax.swing.JLabel txtPlayerXDraw;
    private javax.swing.JLabel txtPlayerXLose;
    private javax.swing.JLabel txtPlayerXWin;
    private javax.swing.JLabel txtStatus;
    // End of variables declaration//GEN-END:variables
    private Player player1, player2;
    private Table table;
    private boolean end;

    private void process(int row, int col) {
        if (!end) {
            if (!table.setRowCol(row, col)) {
                showTable();
                if (table.checkWin() || table.checkDraw()) {
                    showTable();
                    showEnding();
                    showPlayer();
                    end = true;
                } else {
                    table.switchPlayer();
                    showTurn();
                }
            }
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
        txtPlayerX.requestFocus();
        end = false;
        showTable();
        showTurn();
    }
    
    private void showWelcome() {
        txtPlayerXWin.setText("Win: " + player1.getWinCount());
        txtPlayerXLose.setText("Lose: " + player1.getLoseCount());
        txtPlayerXDraw.setText("Draw: " + player1.getDrawCount());
        txtPlayerOWin.setText("Win: " + player2.getWinCount());
        txtPlayerOLose.setText("Lose: " + player2.getLoseCount());
        txtPlayerODraw.setText("Draw: " + player2.getDrawCount());

    }


    private void showTable() {
        char[][] t = table.getTable();
        btnTable00.setText(t[0][0] + "");
        btnTable01.setText(t[0][1] + "");
        btnTable02.setText(t[0][2] + "");
        btnTable10.setText(t[1][0] + "");
        btnTable11.setText(t[1][1] + "");
        btnTable12.setText(t[1][2] + "");
        btnTable20.setText(t[2][0] + "");
        btnTable21.setText(t[2][1] + "");
        btnTable22.setText(t[2][2] + "");
    }

    private void showTurn() {
        txtStatus.setText("Turn " + table.getCurrentPlayer().getSymbol());
    }

    private void showPlayer() {
        txtPlayerXWin.setText("Win: " + player1.getWinCount());
        txtPlayerXLose.setText("Lose: " + player1.getLoseCount());
        txtPlayerXDraw.setText("Draw: " + player1.getDrawCount());
        txtPlayerOWin.setText("Win: " + player2.getWinCount());
        txtPlayerOLose.setText("Lose: " + player2.getLoseCount());
        txtPlayerODraw.setText("Draw: " + player2.getDrawCount());
    }

    private void showEnding() {
        if (table.checkRow() || table.checkCol() || table.checkCross()) {
            txtStatus.setText(table.getCurrentPlayer().getSymbol() + " Win!!");
        } else {
            txtStatus.setText("Draw!!");
        }
    }

}
